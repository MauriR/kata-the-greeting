describe ('The Greeting Kata', () => {
	describe ('Yellow Belt',() => {
		it ('returns a greeting depending on the given name',()=>{
			const salute = new Salute()
			const name = 'Bob'
			const result = salute.greet(name)
			
			expect(result).toEqual('Hello ' + name)
		})
		it ('returns a default output if the given name is null',()=>{
			const salute = new Salute()
			const invalidName = null
			const defaultGreet = 'Hello, my Friend'
			
			const result = salute.greet(invalidName)

			expect(result).toEqual(defaultGreet)
		})
	})
	describe('Green Belt',()=>{
		it('returns an all Uppercase greeting if the given name is Shouted',()=>{
			const salute = new Salute()
			const shoutedName = 'JERRY'
			const shoutedGreet = 'HELLO ' + shoutedName + '!'
			result = salute.greet(shoutedName)

			expect(result).toEqual(shoutedGreet)
		})
	})
	describe('Red Belt',()=>{
		it('allows to introduce two names and greets both of them',()=>{
			const salute = new Salute()
			const twoNames = ["Jill", "Jane"]
			const twoGreets = 'Hello, Jill and Jane'
			
			result = salute.greet(twoNames)
			
			expect(result).toEqual(twoGreets)
		})

		it('allows to greet three names', () =>{
			const salute = new Salute()
			const threeNames = ['Adrian', 'Alvaro', 'Oscar']
			const greetThreePeople = 'Hello, Adrian, Alvaro and Oscar'
			
			result = salute.greet(threeNames)

			expect(result).toEqual(greetThreePeople)
		})

		it('allows to greet any number of names', () =>{
			const salute = new Salute()
			const moreThanThreeNames = ['Adrian', 'Pablo', 'Alvaro', 'Oscar']
			const greetThemAll = 'Hello, Adrian, Pablo, Alvaro and Oscar'
			
			result = salute.greet(moreThanThreeNames)

			expect(result).toEqual(greetThemAll)
		})

		it('mixes normal and shouted names by separating the responses into two greets',()=>{
			const salute = new Salute()
			const mixedSalute = ['Adrian', 'PABLO', 'Alvaro', 'Oscar']
			const separateGreet = 'Hello, Adrian, Alvaro and Oscar. AND HELLO PABLO!'

			result = salute.greet(mixedSalute)

			expect(result).toEqual(separateGreet)
		})
	})
	
})



