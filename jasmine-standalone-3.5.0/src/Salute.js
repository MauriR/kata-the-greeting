const DEFAULT_GREETING = 'Hello, my Friend'
const CAPLETTER =  90

class Salute {
  constructor(){
    this.capitalName = null
    this.minorNames = []
    this.nameFromArray = false
  }

  greet(name){
    if (Array.isArray(name)){
      this.nameFromArray = true
      this.caseSplitting(name)     
      const mixedGreeting = this._saluteEverybody()

      return mixedGreeting
    }
    let greeting = this._justOneName(name)

    return greeting
  }

  _justOneName(name){
    let customGreeting = 'Hello ' + name
    let shoutedGreeting = 'HELLO ' + name +'!'
    let greeting = null

    if (this.nameFromArray){
      if(this._isTheNameInvalid(name)){
        greeting = ''
      }else {
        greeting = '. AND ' + shoutedGreeting
      }
    }else {
      if(this._isTheNameInvalid(name)){
        greeting = DEFAULT_GREETING
      }else if (this._isUpperCase(name)){
        greeting = shoutedGreeting
      }else{
        greeting = customGreeting
      }
    }
    return greeting
  }

  _isTheNameInvalid(name){
    return name == null
  }

  _isUpperCase(name){
    let splitName = name.split("") 
    return splitName.every(letter => letter.charCodeAt() <= CAPLETTER)  
  }

  _greetThemAll(minorNames){
    let adder = minorNames[0]
    for(let i=1; i < minorNames.length -1; i++){
      adder +=', ' + minorNames[i] // adder = adder + ', ' + minorNames[i]
    }
    adder += ' and ' + minorNames[minorNames.length-1]
    return 'Hello, ' +  adder
  }
  
  caseSplitting(name){
    name.forEach(input =>{
      if(this._isUpperCase(input)){
        this.capitalName = input
      }else{
        this.minorNames.push(input)
      }
    })    
  }
  _saluteEverybody(){
    const greetMinorNames = this._greetThemAll(this.minorNames) 
    const greetCapitalName = this._justOneName(this.capitalName) 
    
    return greetMinorNames + greetCapitalName

  }

}

